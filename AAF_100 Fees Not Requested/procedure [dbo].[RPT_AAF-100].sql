USE [ProLaw]
GO
/****** Object:  StoredProcedure [dbo].[RPT_AAF-100]    Script Date: 3/30/2017 11:02:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


ALTER procedure [dbo].[RPT_AAF-100] 

as
/*testing for bitbucket 4/12/2017 saf */
/* Redirect name per Lisa List.  2/24/2017 rle


Ann Marie Falso
Cathleen Casey
Cedric Lunsford
Christine Holmes
Ed Peak
Helen McGannon
Joel Brunswick
John Cook
Jose Lopez
Joseph Sensale
Melissa Santa Maria
Lisa Whittemore


USED:
                              'AnnMarie Falso',
                              'Cathleen Casey',
                              'Cathleen Beaudoin',
                              'Cedric Lunsford',
                              'Christine A. Holmes',
                              'Ed Peak',
                              'Helen McGannon',
                              'Joel Brunswick',  
                              'John Cook',
                              'Jose A. Lopez',
                              'Joseph J. Sensale',
                              'Melissa Santamaria',
                              'Lisa A. Whittemore'

*/

--/*  Replaced 11/30/2015 (rle)
--	Lisa requested for all fees after 7/28/2015
	
--	---------------------------------------------------------------------------------------------------------------
	
----8/22/2014 added in the except for Additional attorney fees not required or additional attorney fees not request,
----these must be a child to the parent event

--/*
--	8/6/2015 Added additional check for non-existance of a completed 'Motion to Exempt from Dormancy Not Needed'
--	per Lisa, Sharepoint task 1289
	
--	Pls amend report to NOT include any file which as a completed event of:  
--	Motion to Exempt from Dormancy Not Needed event which is under the Case Dismissal Process event. Thank you. 
	
--	10/30/2015 LW.
--	Pls amend report to NOT include any file which as a completed event of:  
--	Motion to Open Judgment of Dismissal Drafted event which is under the Case Dismissal Process event. Thank you. 	
	
--*/	

	      Select Distinct 
          M.MatterID as [BMPC File No]  
          ,M.ClientSort as [Client Sort] 
		  ,M.Shortdesc as [Matter Description] 
          ,M.QLoanNumber 
		  ,Case When QCURRENTVESTING is NULL 
				THEN  QCURRENTSALEDAT 
				Else QCurrentVesting 
				END as [Sale-Vesting]
		  ,ParentofRequestedFeeEvent.EventDesc    as [Parent Event]
          ,ParentofRequestedFeeEvent.DoneDate
          ,
			--Case 
			--WHEN M.Status = 'OPEN'
			--THEN
           Case When
		 (	
			 (
			Select TOP 1 P.ProfName from Prolaw.dbo.Professionals P where 
			P.Professionals =Prolaw.dbo.GetEventProf(ParentofRequestedFeeEvent.Events)
			)
			in 
			 (
			'AnnMarie Falso',
			'Cathleen Casey',
			'Cathleen Beaudoin',
			'Cedric Lunsford',
			'Christine A. Holmes',
			'Ed Peak',
			'Helen McGannon',
			'Joel Brunswick',  
			'John Cook',
			'Jose A. Lopez',
			'Joseph J. Sensale',
			'Melissa Santamaria',
			'Lisa A. Whittemore'
			)
			and
			(  /* Force to Nicole if Nicole did Request. */
			Select TOP 1 P.ProfName from Prolaw.dbo.Professionals P 
			inner join Prolaw.dbo.EventProfs EPx on EPx.Professionals = P.Professionals
			and EPX.ProfSet = RequestedFeeEvent.ProfSet)
			! = 'Nicole M. Bruneau'
			)
			THEN
			(Select top 1 P.profName from 
			 Prolaw.dbo.MattersProfessionals MP
			 inner join Prolaw.dbo.Professionals P on P.Professionals = MP.Professionals
				and MP.AssignedType = 'Presale Paralegal')
			ELSE
			(Select TOP 1 P.ProfName from Prolaw.dbo.Professionals P where 
			P.Professionals =Prolaw.dbo.GetEventProf(ParentofRequestedFeeEvent.Events))
			END 
		   ,''
		   ,RequestedFeeEvent.notes

			FROM Prolaw.dbo.Matters M with (nolock)  
			LEFT Join Prolaw.dbo.MattersQForeclosure MQF on M.Matters = MQF.Matters
			inner join 
			(
			Select M.Matters
			From Prolaw.dbo.Matters M
			Where M.AreaOfLaw = 'foreclosure'
				and status in
				(	'open'
					--,'Post Sale confirmation'
					--,'Post Vesting'
					--,'HUD-VA Conveyance'
					--JUST OPEN PER LISA 12/3/2015
				)
			) as WhatMatters on WhatMatters.MAtters = M.Matters
			
		INNER JOIN  
            (  
                Select  
                E.EventsParent as Parent, EM.Matters as Matters, 
                Replace(
				Replace(
					RTRIM(
						LTRIM(
							 Replace(
								Replace(
									Cast(E.notes as varchar(800))
									,cast(ET.DefaultNotes as varchar(200))
									,''
									)
								,'Additional Attorney Fees Requested'
								,''
								)
							 )
						  )
					,char(10)
					,''
					)
				,char(13)
				,''
				) as Notes
				,E.ProfSet
                From Prolaw.dbo.EventMatters Em (nolock)
                inner join Prolaw.dbo.Events E (nolock) on E.Events = Em.Events  
                inner join Prolaw.dbo.EventTypes Et (nolock) on Et.EventTypes=E.EventTypes  
                Where  
                Et.EventDesc = 'Additional Attorney Fees Requested'
                        and E.DoneDate is NULL
				and
				NOT Exists
				(
				Select  
                Ex.Events
                From Prolaw.dbo.Events Ex (nolock)  
                inner join Prolaw.dbo.EventTypes Etx (nolock) on Etx.EventTypes=E.EventTypes  
                Where  
                Et.EventDesc = 'Additional Attorney Fees Not Required'
                and Ex.EventsParent = E.EventsParent
                and E.DoneDate is NOT NULL
                )
				and				
				NOT Exists
				(
				Select  
                Ex.Events
                From Prolaw.dbo.Events Ex (nolock)
                inner join Prolaw.dbo.EventTypes Etx (nolock) on Etx.EventTypes=E.EventTypes  
                Where  
                Etx.EventDesc = 'Additional Attorney Fees Not Requested'  
                and Ex.EventsParent = E.EventsParent  
                and E.DoneDate is NOT NULL 
				)                                 
			  )	As RequestedFeeEvent on RequestedFeeEvent.Matters = WhatMatters.Matters
			  
		  	  
            INNER JOIN  
            (   
                Select  
				EM.Matters, E.EventDate as EventDate, E.Events as Events, Et.EventDesc as EventDesc
				,E.DoneDate
                From Prolaw.dbo.EventMatters Em (nolock)
                inner join Prolaw.dbo.Events E (nolock) on E.Events = Em.Events  
                inner join Prolaw.dbo.EventTypes Et (nolock) on Et.EventTypes=E.EventTypes  
                Where 
		        E.DoneDate is NOT NULL and E.DoneDate > '7/28/2015'
				and
				1 = 
					Case  
						WHEN (ET.EventDesc ='Motion to Exempt from Dormancy Not Needed' and E.DoneDate is NULL)
							then 0
						WHEN (ET.EventDesc = 'Motion to Open Judgment of Dismissal Drafted' and E.DoneDate is NULL	)
							then 0
						ELSE 1
						END
             )  As ParentofRequestedFeeEvent on RequestedFeeEvent.Parent = ParentofRequestedFeeEvent.Events
                 				 
                			  
	Order by 'Parent Event'			  

